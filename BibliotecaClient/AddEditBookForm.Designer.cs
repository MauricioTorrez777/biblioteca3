﻿namespace BibliotecaClient
{
    partial class AddEditBookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTitleBook = new System.Windows.Forms.TextBox();
            this.lTitleBook = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lDateBook = new System.Windows.Forms.Label();
            this.lListAuthor = new System.Windows.Forms.Label();
            this.checkedListBoxAuthor = new System.Windows.Forms.CheckedListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbTitleBook
            // 
            this.tbTitleBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTitleBook.Location = new System.Drawing.Point(103, 59);
            this.tbTitleBook.Name = "tbTitleBook";
            this.tbTitleBook.Size = new System.Drawing.Size(321, 30);
            this.tbTitleBook.TabIndex = 0;
            // 
            // lTitleBook
            // 
            this.lTitleBook.AutoSize = true;
            this.lTitleBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTitleBook.Location = new System.Drawing.Point(12, 62);
            this.lTitleBook.Name = "lTitleBook";
            this.lTitleBook.Size = new System.Drawing.Size(60, 25);
            this.lTitleBook.TabIndex = 1;
            this.lTitleBook.Text = "Titulo";
            this.lTitleBook.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(103, 111);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(321, 30);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // lDateBook
            // 
            this.lDateBook.AutoSize = true;
            this.lDateBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDateBook.Location = new System.Drawing.Point(12, 116);
            this.lDateBook.Name = "lDateBook";
            this.lDateBook.Size = new System.Drawing.Size(76, 25);
            this.lDateBook.TabIndex = 3;
            this.lDateBook.Text = "Edicion";
            // 
            // lListAuthor
            // 
            this.lListAuthor.AutoSize = true;
            this.lListAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lListAuthor.Location = new System.Drawing.Point(548, 17);
            this.lListAuthor.Name = "lListAuthor";
            this.lListAuthor.Size = new System.Drawing.Size(153, 25);
            this.lListAuthor.TabIndex = 4;
            this.lListAuthor.Text = "Lista de Autores";
            // 
            // checkedListBoxAuthor
            // 
            this.checkedListBoxAuthor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBoxAuthor.FormattingEnabled = true;
            this.checkedListBoxAuthor.Location = new System.Drawing.Point(453, 59);
            this.checkedListBoxAuthor.Name = "checkedListBoxAuthor";
            this.checkedListBoxAuthor.Size = new System.Drawing.Size(350, 379);
            this.checkedListBoxAuthor.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(12, 399);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(126, 45);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(144, 399);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(126, 45);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AddEditBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 456);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.checkedListBoxAuthor);
            this.Controls.Add(this.lListAuthor);
            this.Controls.Add(this.lDateBook);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.lTitleBook);
            this.Controls.Add(this.tbTitleBook);
            this.Name = "AddEditBookForm";
            this.Text = "Agregar/Editar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTitleBook;
        private System.Windows.Forms.Label lTitleBook;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lDateBook;
        private System.Windows.Forms.Label lListAuthor;
        private System.Windows.Forms.CheckedListBox checkedListBoxAuthor;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}

