USE [Biblioteca]
GO
/****** Object:  StoredProcedure [dbo].[SP_addBook]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_addBook] 
	@titulo 		NVARCHAR(255) = NULL,
	@edicion 	Datetime = NULL,
	@idAutor 	NVARCHAR(255) = NULL
AS
BEGIN

	SET NOCOUNT ON;

		INSERT INTO libro (titulo, fecha_edicion) values (@titulo, @edicion)
		--INSERT INTO Autor_libro(id_libro, id_autor) values (@@IDENTITY, 1)

		--SELECT @@IDENTITY AS 'Identity';
		declare @idLibro int  = @@IDENTITY;


--delete libro where id_libro=4

declare @String  VARCHAR(MAX) = @idAutor
declare  @Separator CHAR(1) = ','
declare @RESULT TABLE(Value VARCHAR(MAX))

DECLARE @SeparatorPosition INT = CHARINDEX(@Separator, @String ),
        @Value VARCHAR(MAX), @StartPosition INT = 1

		print  @SeparatorPosition
 
 IF @SeparatorPosition = 0  
  BEGIN
   INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@String AS NUMERIC(19,4))))
   RETURN
  END
     
 SET @String = @String + @Separator
 WHILE @SeparatorPosition > 0
  BEGIN
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)
 
   IF( @Value <> ''  ) 
    INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@Value AS NUMERIC(19,4))))
   
   SET @StartPosition = @SeparatorPosition + 1
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)
  END    

END 
GO
/****** Object:  StoredProcedure [dbo].[SP_editBook]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_editBook] 
	@idLibro INT ,
	@titulo 		NVARCHAR(255) = NULL,
	@edicion 	Datetime = NULL,
	@idAutor 	NVARCHAR(255) = NULL
AS
BEGIN

	SET NOCOUNT ON;

		UPDATE libro set titulo = @titulo, fecha_edicion=@edicion WHERE id_libro = @idLibro

		delete autor_libro where id_libro = @idLibro


--delete libro where id_libro=4

declare @String  VARCHAR(MAX) = @idAutor
declare  @Separator CHAR(1) = ','
declare @RESULT TABLE(Value VARCHAR(MAX))

DECLARE @SeparatorPosition INT = CHARINDEX(@Separator, @String ),
        @Value VARCHAR(MAX), @StartPosition INT = 1

		print  @SeparatorPosition
 
 IF @SeparatorPosition = 0  
  BEGIN
   INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@String AS NUMERIC(19,4))))
   RETURN
  END
     
 SET @String = @String + @Separator
 WHILE @SeparatorPosition > 0
  BEGIN
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)
 
   IF( @Value <> ''  ) 
    INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@Value AS NUMERIC(19,4))))
   
   SET @StartPosition = @SeparatorPosition + 1
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)
  END    

END 
GO
/****** Object:  StoredProcedure [dbo].[SP_getAllAuthors]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_getAllAuthors] 
AS
BEGIN

	SET NOCOUNT ON;

		SELECT id_autor, nombre
		FROM autor
END
GO
/****** Object:  StoredProcedure [dbo].[SP_getAllBooks]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_getAllBooks] 
AS
BEGIN

	SET NOCOUNT ON;

		SELECT l.id_libro, l.titulo,fecha_edicion, COUNT (l.id_libro) as Autor
		FROM Libro l, autor_libro al
		WHERE l.id_libro = al.id_libro
		GROUP BY l.id_libro, l.titulo,fecha_edicion 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_getAuthorsfromId]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_getAuthorsfromId] 
@idLibro INT 
AS
BEGIN

		SELECT id_autor
		FROM autor_libro
		WHERE id_libro=@idLibro
END
GO
/****** Object:  StoredProcedure [dbo].[SP_getBooks]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_getBooks] 
	@titulo 		NVARCHAR(255) = NULL,
	@fecha 	Datetime = NULL,
	@autorId 	int = NULL
AS
BEGIN

	SET NOCOUNT ON;

		SELECT l.id_libro, l.titulo,fecha_edicion, COUNT (l.id_libro) as Autor
		FROM Libro l, autor_libro al
		WHERE l.id_libro = al.id_libro AND l.titulo = @titulo AND (@fecha >= l.fecha_edicion OR @fecha <= l.fecha_edicion) AND @autorId in (select id_autor from autor_libro where al.id_libro=l.id_libro)
		GROUP BY l.id_libro, l.titulo,fecha_edicion 
END
GO
/****** Object:  Table [dbo].[autor]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[autor](
	[id_autor] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nchar](255) NULL,
 CONSTRAINT [PK_autor] PRIMARY KEY CLUSTERED 
(
	[id_autor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[autor_libro]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[autor_libro](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_autor] [int] NOT NULL,
	[id_libro] [int] NOT NULL,
 CONSTRAINT [PK_autor_libro] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[libro]    Script Date: 3/17/2019 9:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[libro](
	[id_libro] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nchar](255) NULL,
	[fecha_edicion] [datetime2](7) NULL,
 CONSTRAINT [PK_libro] PRIMARY KEY CLUSTERED 
(
	[id_libro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
