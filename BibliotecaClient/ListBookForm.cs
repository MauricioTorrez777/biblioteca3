﻿using DTO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;

namespace BibliotecaClient
{
    public partial class ListBookForm : Form
    {
        BibliotecaHelper helper;
        public ListBookForm()
        {
            InitializeComponent();
            helper = new BibliotecaHelper();
            RefreshGrid();
            populateAuthors();
        }

        private void populateAuthors()
        {
            var ds = helper.getAllAutors();
            comboBoxAutores.DataSource = ds.Tables[0];
            comboBoxAutores.DisplayMember = "nombre";
            comboBoxAutores.ValueMember = "id_autor";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.dataGridViewBooks.DataSource = null;
            this.dataGridViewBooks.Refresh();
            this.dataGridViewBooks.DataSource = helper.GetBooks(this.tbBookName.Text, this.dateTimePBook.Value, (int)this.comboBoxAutores.SelectedValue).Tables[0];
            this.dataGridViewBooks.Refresh();
        }

        private void btnNewBook_Click(object sender, EventArgs e)
        {
            AddEditBookForm addEditBook = new AddEditBookForm();
            addEditBook.Show();
        }

        private void dataGridViewBooks_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(e.RowIndex.ToString());
        }

        private void dataGridViewBooks_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {            
            try
            {
                var dataIndexNo = dataGridViewBooks.Rows[e.RowIndex].Index.ToString();
                string cellValue = dataGridViewBooks.Rows[e.RowIndex].Cells[0].Value.ToString();
                MessageBox.Show(cellValue);
                Libro book = new Libro() { id_libro = Int32.Parse(cellValue), titulo = dataGridViewBooks.Rows[e.RowIndex].Cells[1].Value.ToString().Trim(), fecha_edicion = DateTime.Parse(dataGridViewBooks.Rows[e.RowIndex].Cells[2].Value.ToString()), idAutor = helper.GetAuthorsfromId(Int32.Parse(cellValue)) }; ;
                AddEditBookForm editBook = new AddEditBookForm(book);
                editBook.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RefreshGrid()
        {
            this.dataGridViewBooks.DataSource = null;
            this.dataGridViewBooks.Refresh();
            this.dataGridViewBooks.DataSource = helper.GetALLBooks().Tables[0];
            this.dataGridViewBooks.Refresh();
        }


    }
}
