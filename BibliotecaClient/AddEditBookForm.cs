﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaClient
{
    public partial class AddEditBookForm : Form
    {
        BibliotecaHelper helper;
        public bool isEdit = false;
        public int currentBook = -1;
        public AddEditBookForm()
        {
            InitializeComponent();
            helper = new BibliotecaHelper();
            PopulateAutores();
        }

        public AddEditBookForm(Libro book)
        {
            InitializeComponent();
            helper = new BibliotecaHelper();
            currentBook = book.id_libro;
            PopulateAutores();
            PopulateData(book);
            isEdit = true;
        }

        private void PopulateData(Libro book)
        {
            this.tbTitleBook.Text = book.titulo;
            this.dateTimePicker1.Value = book.fecha_edicion;
            CheckAuthors(book.idAutor);
        }

        private void CheckAuthors(string p)
        {
            var arrayAuth = p.Split(',');
            foreach (var item in arrayAuth)
            {

                for (int i = 0; i < this.checkedListBoxAuthor.Items.Count; i++)
                {
                    DataRowView castedItem = this.checkedListBoxAuthor.Items[i] as DataRowView;
                    if (item.Trim() == castedItem["id_autor"].ToString().Trim())
                    {
                        this.checkedListBoxAuthor.SetItemCheckState(i, (true ? CheckState.Checked : CheckState.Unchecked));
                    }
                    
                }

            }
        }

        private void PopulateAutores()
        {
            var ds = helper.getAllAutors();
            ((ListBox)this.checkedListBoxAuthor).DataSource = ds.Tables[0];
            ((ListBox)this.checkedListBoxAuthor).DisplayMember = "nombre";
            ((ListBox)this.checkedListBoxAuthor).ValueMember = "id_autor";

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Libro lib = new Libro() { id_libro=currentBook,titulo = this.tbTitleBook.Text, fecha_edicion = DateTime.Parse(this.dateTimePicker1.Text), idAutor = GetSelectedAuthors() };
            if (isEdit)
            {
                helper.UpdateBook(lib);
            }
            else
            {                
                helper.AddBook(lib);
            }
            this.Close();
        }

        private string GetSelectedAuthors()
        {
            string checkedAuthors = string.Empty;
            if (this.checkedListBoxAuthor.CheckedItems.Count > 0)
            {                
                foreach (var itemChecked in this.checkedListBoxAuthor.CheckedItems)
                {
                    DataRowView castedItem = itemChecked as DataRowView;
                    checkedAuthors += castedItem["id_autor"] + ",";
                }
                checkedAuthors = checkedAuthors.Remove(checkedAuthors.Length - 1);
            }
            return checkedAuthors;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
