﻿namespace BibliotecaClient
{
    partial class ListBookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.dataGridViewBooks = new System.Windows.Forms.DataGridView();
            this.dateTimePBook = new System.Windows.Forms.DateTimePicker();
            this.tbBookName = new System.Windows.Forms.TextBox();
            this.lTileBook = new System.Windows.Forms.Label();
            this.lAuthorName = new System.Windows.Forms.Label();
            this.btnNewBook = new System.Windows.Forms.Button();
            this.lDateBook = new System.Windows.Forms.Label();
            this.comboBoxAutores = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBooks)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(698, 62);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(135, 30);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Buscar";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dataGridViewBooks
            // 
            this.dataGridViewBooks.AllowUserToAddRows = false;
            this.dataGridViewBooks.AllowUserToDeleteRows = false;
            this.dataGridViewBooks.AllowUserToOrderColumns = true;
            this.dataGridViewBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBooks.Location = new System.Drawing.Point(17, 124);
            this.dataGridViewBooks.Name = "dataGridViewBooks";
            this.dataGridViewBooks.ReadOnly = true;
            this.dataGridViewBooks.RowTemplate.Height = 24;
            this.dataGridViewBooks.Size = new System.Drawing.Size(816, 288);
            this.dataGridViewBooks.TabIndex = 1;
            this.dataGridViewBooks.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBooks_CellContentDoubleClick);
            this.dataGridViewBooks.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewBooks_CellDoubleClick);
            // 
            // dateTimePBook
            // 
            this.dateTimePBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePBook.Location = new System.Drawing.Point(94, 62);
            this.dateTimePBook.Name = "dateTimePBook";
            this.dateTimePBook.Size = new System.Drawing.Size(325, 30);
            this.dateTimePBook.TabIndex = 2;
            // 
            // tbBookName
            // 
            this.tbBookName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBookName.Location = new System.Drawing.Point(94, 12);
            this.tbBookName.Name = "tbBookName";
            this.tbBookName.Size = new System.Drawing.Size(325, 30);
            this.tbBookName.TabIndex = 3;
            // 
            // lTileBook
            // 
            this.lTileBook.AutoSize = true;
            this.lTileBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTileBook.Location = new System.Drawing.Point(12, 15);
            this.lTileBook.Name = "lTileBook";
            this.lTileBook.Size = new System.Drawing.Size(60, 25);
            this.lTileBook.TabIndex = 5;
            this.lTileBook.Text = "Titulo";
            // 
            // lAuthorName
            // 
            this.lAuthorName.AutoSize = true;
            this.lAuthorName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lAuthorName.Location = new System.Drawing.Point(443, 15);
            this.lAuthorName.Name = "lAuthorName";
            this.lAuthorName.Size = new System.Drawing.Size(59, 25);
            this.lAuthorName.TabIndex = 6;
            this.lAuthorName.Text = "Autor";
            // 
            // btnNewBook
            // 
            this.btnNewBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewBook.Location = new System.Drawing.Point(669, 434);
            this.btnNewBook.Name = "btnNewBook";
            this.btnNewBook.Size = new System.Drawing.Size(164, 35);
            this.btnNewBook.TabIndex = 7;
            this.btnNewBook.Text = "Agregar Libro";
            this.btnNewBook.UseVisualStyleBackColor = true;
            this.btnNewBook.Click += new System.EventHandler(this.btnNewBook_Click);
            // 
            // lDateBook
            // 
            this.lDateBook.AutoSize = true;
            this.lDateBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDateBook.Location = new System.Drawing.Point(12, 65);
            this.lDateBook.Name = "lDateBook";
            this.lDateBook.Size = new System.Drawing.Size(76, 25);
            this.lDateBook.TabIndex = 8;
            this.lDateBook.Text = "Edicion";
            // 
            // comboBoxAutores
            // 
            this.comboBoxAutores.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxAutores.FormattingEnabled = true;
            this.comboBoxAutores.Location = new System.Drawing.Point(508, 12);
            this.comboBoxAutores.Name = "comboBoxAutores";
            this.comboBoxAutores.Size = new System.Drawing.Size(325, 33);
            this.comboBoxAutores.TabIndex = 9;
            // 
            // ListBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 481);
            this.Controls.Add(this.comboBoxAutores);
            this.Controls.Add(this.lDateBook);
            this.Controls.Add(this.btnNewBook);
            this.Controls.Add(this.lAuthorName);
            this.Controls.Add(this.lTileBook);
            this.Controls.Add(this.tbBookName);
            this.Controls.Add(this.dateTimePBook);
            this.Controls.Add(this.dataGridViewBooks);
            this.Controls.Add(this.btnSearch);
            this.Name = "ListBookForm";
            this.Text = "Listar Libros";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBooks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dataGridViewBooks;
        private System.Windows.Forms.DateTimePicker dateTimePBook;
        private System.Windows.Forms.TextBox tbBookName;
        private System.Windows.Forms.Label lTileBook;
        private System.Windows.Forms.Label lAuthorName;
        private System.Windows.Forms.Button btnNewBook;
        private System.Windows.Forms.Label lDateBook;
        private System.Windows.Forms.ComboBox comboBoxAutores;
    }
}