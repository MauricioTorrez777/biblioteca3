﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Libro
    {
        public int id_libro { get; set; }
        public string titulo { get; set; }
        public DateTime fecha_edicion { get; set; }
        public string idAutor { get; set; }
    }
}
