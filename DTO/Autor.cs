﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Autor
    {
        public int id_autor { get; set; }
        public string nombre { get; set; }
    }
}
