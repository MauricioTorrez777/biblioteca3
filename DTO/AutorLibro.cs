﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class AutorLibro
    {
        public int id { get; set; }
        public int id_autor { get; set; }
        public int id_libro { get; set; }
    }
}
