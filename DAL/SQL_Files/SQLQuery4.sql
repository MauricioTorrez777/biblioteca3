
CREATE PROCEDURE [dbo].[SP_getBooks] 
	@titulo 		NVARCHAR(255) = NULL,
	@fecha 	Datetime = NULL,
	@autorId 	int = NULL
AS
BEGIN

	SET NOCOUNT ON;

		SELECT l.id_libro, l.titulo,fecha_edicion, COUNT (l.id_libro) as Autor
		FROM Libro l, autor_libro al
		WHERE l.id_libro = al.id_libro AND l.titulo = @titulo AND (@fecha >= l.fecha_edicion OR @fecha <= l.fecha_edicion) AND @autorId in (select id_autor from autor_libro where al.id_libro=l.id_libro)
		GROUP BY l.id_libro, l.titulo,fecha_edicion 
END



CREATE PROCEDURE [dbo].[SP_getAllBooks] 
AS
BEGIN

	SET NOCOUNT ON;

		SELECT l.id_libro, l.titulo,fecha_edicion, COUNT (l.id_libro) as Autor, 
		FROM Libro l, autor_libro al
		WHERE l.id_libro = al.id_libro
		GROUP BY l.id_libro, l.titulo,fecha_edicion 
END
--SP_addBook


ALTER PROCEDURE [dbo].[SP_addBook] 
	@titulo 		NVARCHAR(255) = NULL,
	@edicion 	Datetime = NULL,
	@idAutor 	NVARCHAR(255) = NULL
AS
BEGIN

	SET NOCOUNT ON;

		INSERT INTO libro (titulo, fecha_edicion) values (@titulo, @edicion)
		--INSERT INTO Autor_libro(id_libro, id_autor) values (@@IDENTITY, 1)

		--SELECT @@IDENTITY AS 'Identity';
		declare @idLibro int  = @@IDENTITY;


--delete libro where id_libro=4

declare @String  VARCHAR(MAX) = @idAutor
declare  @Separator CHAR(1) = ','
declare @RESULT TABLE(Value VARCHAR(MAX))

DECLARE @SeparatorPosition INT = CHARINDEX(@Separator, @String ),
        @Value VARCHAR(MAX), @StartPosition INT = 1

		print  @SeparatorPosition
 
 IF @SeparatorPosition = 0  
  BEGIN
   INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@String AS NUMERIC(19,4))))
   RETURN
  END
     
 SET @String = @String + @Separator
 WHILE @SeparatorPosition > 0
  BEGIN
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)
 
   IF( @Value <> ''  ) 
    INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@Value AS NUMERIC(19,4))))
   
   SET @StartPosition = @SeparatorPosition + 1
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)
  END    

END 




delete autor_libro where id=8 or id=7


select * from autor_libro







CREATE PROCEDURE [dbo].[SP_editBook] 
	@idLibro INT ,
	@titulo 		NVARCHAR(255) = NULL,
	@edicion 	Datetime = NULL,
	@idAutor 	NVARCHAR(255) = NULL
AS
BEGIN

	SET NOCOUNT ON;

		UPDATE libro set titulo = @titulo, fecha_edicion=@edicion WHERE id_libro = @idLibro

		delete autor_libro where id_libro = @idLibro


--delete libro where id_libro=4

declare @String  VARCHAR(MAX) = @idAutor
declare  @Separator CHAR(1) = ','
declare @RESULT TABLE(Value VARCHAR(MAX))

DECLARE @SeparatorPosition INT = CHARINDEX(@Separator, @String ),
        @Value VARCHAR(MAX), @StartPosition INT = 1

		print  @SeparatorPosition
 
 IF @SeparatorPosition = 0  
  BEGIN
   INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@String AS NUMERIC(19,4))))
   RETURN
  END
     
 SET @String = @String + @Separator
 WHILE @SeparatorPosition > 0
  BEGIN
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)
 
   IF( @Value <> ''  ) 
    INSERT INTO autor_libro (id_libro, id_autor) VALUES(@idLibro,(CAST (@Value AS NUMERIC(19,4))))
   
   SET @StartPosition = @SeparatorPosition + 1
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)
  END    

END 




SP_getAllAuthors


CREATE PROCEDURE [dbo].[SP_getAllAuthors] 
AS
BEGIN

	SET NOCOUNT ON;

		SELECT id_autor, nombre
		FROM autor
END


select * from autor_libro
select * from libro



--SP_getAuthorsfromId


CREATE PROCEDURE [dbo].[SP_getAuthorsfromId] 
@idLibro INT 
AS
BEGIN

		SELECT id_autor
		FROM autor_libro
		WHERE id_libro=@idLibro
END