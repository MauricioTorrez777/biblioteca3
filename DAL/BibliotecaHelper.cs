﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using DTO;
using System.Data;

namespace DAL
{
    public class BibliotecaHelper
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bibliotecaConn"].ConnectionString);


        public void AddBook(Libro book)
        {
            SqlCommand sp_com = new SqlCommand("SP_addBook", con);
            sp_com.CommandType = System.Data.CommandType.StoredProcedure;
            sp_com.Parameters.AddWithValue("@titulo",book.titulo);
            sp_com.Parameters.AddWithValue("@edicion", book.fecha_edicion);
            sp_com.Parameters.AddWithValue("@idAutor", book.idAutor);
            con.Open();
            sp_com.ExecuteNonQuery();
            con.Close();
        }

        public void UpdateBook(Libro book)
        {
            SqlCommand sp_com = new SqlCommand("SP_editBook", con);
            sp_com.CommandType = System.Data.CommandType.StoredProcedure;
            sp_com.Parameters.AddWithValue("@idLibro", book.id_libro);
            sp_com.Parameters.AddWithValue("@titulo", book.titulo);
            sp_com.Parameters.AddWithValue("@edicion", book.fecha_edicion);
            sp_com.Parameters.AddWithValue("@idAutor", book.idAutor);
            con.Open();
            sp_com.ExecuteNonQuery();
            con.Close();
        }

        public DataSet GetBooks(string titulo, DateTime fecha, int autorId)
        {
            SqlCommand sp_com = new SqlCommand("SP_getBooks", con);
            sp_com.CommandType = System.Data.CommandType.StoredProcedure;
            sp_com.Parameters.AddWithValue("@titulo", titulo);
            sp_com.Parameters.AddWithValue("@fecha", fecha);
            sp_com.Parameters.AddWithValue("@autorId", autorId);
            SqlDataAdapter da = new SqlDataAdapter(sp_com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public DataSet GetALLBooks()
        {
            SqlCommand sp_com = new SqlCommand("SP_getAllBooks", con);
            sp_com.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(sp_com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }


        public DataSet getAllAutors()
        {
            SqlCommand sp_com = new SqlCommand("SP_getAllAuthors", con);
            sp_com.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(sp_com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public string GetAuthorsfromId(int idBook)
        {
            SqlCommand sp_com = new SqlCommand("SP_getAuthorsfromId", con);
            sp_com.Parameters.AddWithValue("@idLibro", idBook);
            sp_com.CommandType = System.Data.CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(sp_com);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string checkedAuthors = string.Empty;
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    checkedAuthors += item["id_autor"] + ",";
                }
                checkedAuthors = checkedAuthors.Remove(checkedAuthors.Length - 1);
            }
            return checkedAuthors;
        }
    }
}
